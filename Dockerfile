FROM node
WORKDIR /usr/src/
COPY . .
EXPOSE 3000
CMD node server.js
